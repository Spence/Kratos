//    |  /           |
//    ' /   __| _` | __|  _ \   __|
//    . \  |   (   | |   (   |\__ \.
//   _|\_\_|  \__,_|\__|\___/ ____/
//                   Multi-Physics
//
//  License:		 BSD License
//					 Kratos default license: kratos/license.txt
//
//  Main authors:    Vicenet Mataix Ferrandiz
//

#if !defined(KRATOS_ADD_MAPPING_VARIABLES_TO_PYTHON_H_INCLUDED)
#define  KRATOS_ADD_MAPPING_VARIABLES_TO_PYTHON_H_INCLUDED

// System includes


// External includes


// Project includes


namespace Kratos
{

namespace Python
{

void  AddMappingVariablesToPython();

}  // namespace Python.

}  // namespace Kratos.

#endif // KRATOS_ADD_MAPPING_VARIABLES_TO_PYTHON_H_INCLUDED  defined
