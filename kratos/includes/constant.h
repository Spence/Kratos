//    |  /           |
//    ' /   __| _` | __|  _ \   __|
//    . \  |   (   | |   (   |\__ `
//   _|\_\_|  \__,_|\__|\___/ ____/
//                   Multi-Physics 
//
//  License:		 BSD License 
//					 Kratos default license: kratos/license.txt
//
//  Main authors:    Pooyan Dadvand
//                    
//

#if !defined(KRATOS_CONSTANTS )
#define  KRATOS_CONSTANTS

const double KRATOS_M_PI 	= 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651L;
const double KRATOS_M_PI_2 	= 1.57079632679489661923132169163975144209858469968755291048747229615390820314310449931401741267105853399107404326L;
const double KRATOS_M_PI_3 	= 1.04719755119659774615421446109316762806572313312503527365831486410260546876206966620934494178070568932738269550L;

#endif // KRATOS_CONSTANTS defined 



