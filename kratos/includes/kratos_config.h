//    |  /           |
//    ' /   __| _` | __|  _ \   __|
//    . \  |   (   | |   (   |\__ `
//   _|\_\_|  \__,_|\__|\___/ ____/
//                   Multi-Physics 
//
//  License:		 BSD License 
//					 Kratos default license: kratos/license.txt
//
//  Main authors:    Pooyan Dadvand
//                   Riccardo Rossi
//                    
//




#if !defined(KRATOS_CONFIG_H_INCLUDED )

#define  KRATOS_CONFIG_H_INCLUDED



// To set the exception handling level in kratos from 1 to 4.

// 1 is just for outer function while 4 will activate them in all places.



#define KRATOS_SET_EXCEPTION_LEVEL_TO_1

//#define KRATOS_ADD_HERMITIAN_MATRIX_INTERFACE



#endif // KRATOS_CONFIG_H_INCLUDED  defined 

